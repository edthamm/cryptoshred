Quickstart Guide
================

Configure
---------

You can configure Cryptoshred via environment variables or configuration files.

You will need

- ``DYNAMO_BACKEND_IV_PARAM`` (required): The SSM parameter that holds your initialization vector

and if the dynamo table that holds your key has a name other than ``cryptoshred-keys``
you will need to set:

- ``DYNAMO_BACKEND_TABLE_NAME``: The name of the table holding the keys

Details on how to configure Cryptoshred can be found here: :ref:`cli/configuration:Configuration`.
Using a configuration file based approach is recommended for CLI use.

Use
---

The CLI app is described in this part of the documentation: :ref:`cli/cli_reference:Command Line Reference`.

The library can be used programmatically as you would any other python lib.
