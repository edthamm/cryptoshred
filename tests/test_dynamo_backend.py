from uuid import uuid4
import pytest
from cryptoshred.backends import DynamoDbSsmBackend
from cryptoshred.exceptions import KeyNotFoundException


def test_empty_result(cryptoshred_table, iv_ssm, dynamodb):
    backend = DynamoDbSsmBackend(
        iv_param="/initialization-vector",
        table_name=cryptoshred_table.table_name,
        dynamo=dynamodb,
    )

    with pytest.raises(KeyNotFoundException):
        backend.get_key(id=uuid4())
