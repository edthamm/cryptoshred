from pydantic import ValidationError
import pytest
from cryptoshred.config import get_configuration


def test_call_with_no_profile_and_no_env():
    with pytest.raises(ValidationError):
        get_configuration(profile=None)
