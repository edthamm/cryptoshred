import json
from pathlib import Path
import cryptoshred.app.ui.map as cui


def test_list_to_stdout(static_source_context, capsys):
    cui.list(
        ctx=static_source_context,
        input=Path(__file__).parent.joinpath("../../resources/list_of_containers.json"),
        out=None,
    )

    captured = capsys.readouterr()
    assert "John" in captured.out
    assert "2134cad1-2ef5-449c-b66c-ca05e7ba3caa" in captured.out


def test_list_to_file(static_source_context, tmpdir):
    res_file = tmpdir.join("test_sid_to_file_mapper.json")

    cui.list(
        ctx=static_source_context,
        input=Path(__file__).parent.joinpath("../../resources/list_of_containers.json"),
        out=res_file,
    )

    with open(res_file, "r") as f:
        res = json.load(f)

    assert ["John"] in res.values()
    assert "f2cd9512-f45e-4ef2-980c-fa220d716a29" in res.keys()
