from uuid import uuid4

import pytest
import typer
import cryptoshred.app.ui.encrypt as cui


def test_encrypt(static_source_context, capsys):
    cui.value(ctx=static_source_context, sid=uuid4(), value="4")
    captured = capsys.readouterr()
    assert "enc" in captured.out


def test_encrypt_empty(empty_source_context, capsys):
    with pytest.raises(typer.Abort):
        cui.value(ctx=empty_source_context, sid=uuid4(), value="4")

    captured = capsys.readouterr()
    assert "Could not find key for provided ID" in captured.out
