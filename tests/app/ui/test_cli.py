from typer.testing import CliRunner

from cryptoshred.app.ui.cli import app

runner = CliRunner()


def test_has_a_verbose_option():
    result = runner.invoke(app, "--help")
    assert result.exit_code == 0
    assert "verbose" in result.stdout
