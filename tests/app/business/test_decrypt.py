from pathlib import Path
from uuid import uuid4
from cryptoshred.app.business.decrypt import (
    from_dict_in_file,
    from_json_string,
    from_list_in_file,
)
from cryptoshred.entities import container_for


def test_string(static_backend):
    id = uuid4()
    res = from_json_string(
        container_for(5, id=id, key_backend=static_backend).json(), static_backend
    )
    assert res == "\x05"


def test_list_in_file(static_backend):
    res = from_list_in_file(
        path=Path(__file__).parent.joinpath("../../resources/list_of_containers.json"),
        key_backend=static_backend,
    )
    assert res == ["John", "10", "\n"]


def test_from_dict_in_file(static_backend):
    res = from_dict_in_file(
        path=Path(__file__).parent.joinpath(
            "../../resources/random_dict_with_container.json"
        ),
        key_backend=static_backend,
    )
    assert res[0]["self"][0]["name"] == "John"
