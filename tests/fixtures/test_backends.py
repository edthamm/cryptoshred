from uuid import uuid4
import pytest
from base64 import b64decode

from cryptoshred.exceptions import KeyNotFoundException


class StaticBackend:
    def get_key(self, id):
        return (id, b64decode("HrHioKGYe0VNmuLXHXwdL/N+oKSdQ/7uTQFXFKU9b9o="))

    def get_iv(self):
        return b"nT26prpVGKF5WFdo"

    def generate_key(self):
        return uuid4()


class EmptyBackend:
    def get_key(self, id):
        raise KeyNotFoundException()

    def get_iv(self):
        return b"nT26prpVGKF5WFdo"


@pytest.fixture(scope="session")
def static_backend():
    return StaticBackend()


@pytest.fixture(scope="session")
def empty_backend():
    return EmptyBackend()
